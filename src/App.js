import React, { useState } from 'react';
import { createWorker } from 'tesseract.js';
import PropTypes from 'prop-types';
import Camera, { FACING_MODES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';

var canvas = document.createElement('canvas');
var context = canvas.getContext('2d');
const dWidth = 352;
const dHeight = 89;
const dx = 66; 
const dy = 276;
var thresholdLevel = 0.8;
var blurARGBradius = 1;
var blurARGBVal;
var dilateVal;
var invertColorsVal;
var thresholdFilterVal;
var processToGrayImageVal;
var oTSUAlgorithmVal;

function setThresholdLevel(val){thresholdLevel = val;}
function setBlurRadiusVal(val){blurARGBradius = val;}
function setBlurARGB(val){blurARGBVal = val;}
function setDilate(val){dilateVal = val;}
function setInvertColors(val){invertColorsVal = val;}
function setThresholdFilter(val){thresholdFilterVal = val;}
function setProcessToGrayImage(val){processToGrayImageVal = val;}
function setOTSUAlgorithm(val){oTSUAlgorithmVal = val;}

function convertURIToImageData(URI) {
  return new Promise(function(resolve, reject) {
    if (URI == null) return reject();
    var image = new Image();
    image.addEventListener('load', function() {
	  canvas.width = image.width;
	  canvas.height = image.height;
      context.drawImage(image, 0, 0, canvas.width, canvas.height );
      resolve(context.getImageData(dx, dy, dWidth, dHeight));
    }, false);
    image.src = URI;
  });
}

function getARGB (data, i) {
  const offset = i * 4;
  return (
    ((data[offset + 3] << 24) & 0xff000000) |
    ((data[offset] << 16) & 0x00ff0000) |
    ((data[offset + 1] << 8) & 0x0000ff00) |
    (data[offset + 2] & 0x000000ff)
  );
};

function setPixels (pixels, data) {
  let offset = 0;
  for (let i = 0, al = pixels.length; i < al; i++) {
    offset = i * 4;
    pixels[offset + 0] = (data[i] & 0x00ff0000) >>> 16;
    pixels[offset + 1] = (data[i] & 0x0000ff00) >>> 8;
    pixels[offset + 2] = data[i] & 0x000000ff;
    pixels[offset + 3] = (data[i] & 0xff000000) >>> 24;
  }
};

// internal kernel stuff for the gaussian blur filter
  let blurRadius;
  let blurKernelSize;
  let blurKernel;
  let blurMult;

  // from https://github.com/processing/p5.js/blob/main/src/image/filters.js
  function buildBlurKernel(r) {
  let radius = (r * 3.5) | 0;
  radius = radius < 1 ? 1 : radius < 248 ? radius : 248;

  if (blurRadius !== radius) {
    blurRadius = radius;
    blurKernelSize = (1 + blurRadius) << 1;
    blurKernel = new Int32Array(blurKernelSize);
    blurMult = new Array(blurKernelSize);
    for (let l = 0; l < blurKernelSize; l++) {
      blurMult[l] = new Int32Array(256);
    }

    let bk, bki;
    let bm, bmi;

    for (let i = 1, radiusi = radius - 1; i < radius; i++) {
      blurKernel[radius + i] = blurKernel[radiusi] = bki = radiusi * radiusi;
      bm = blurMult[radius + i];
      bmi = blurMult[radiusi--];
      for (let j = 0; j < 256; j++) {
        bm[j] = bmi[j] = bki * j;
      }
    }
    bk = blurKernel[radius] = radius * radius;
    bm = blurMult[radius];

    for (let k = 0; k < 256; k++) {
      bm[k] = bk * k;
    }
  }
}

function blurARGB(pixels, canvas, radius) {
  const width = canvas.width;
  const height = canvas.height;
  const numPackedPixels = width * height;
  const argb = new Int32Array(numPackedPixels);
  for (let j = 0; j < numPackedPixels; j++) {
    argb[j] = getARGB(pixels, j);
  }
  let sum, cr, cg, cb, ca;
  let read, ri, ym, ymi, bk0;
  const a2 = new Int32Array(numPackedPixels);
  const r2 = new Int32Array(numPackedPixels);
  const g2 = new Int32Array(numPackedPixels);
  const b2 = new Int32Array(numPackedPixels);
  let yi = 0;
  buildBlurKernel(radius);
  let x, y, i;
  let bm;
  for (y = 0; y < height; y++) {
    for (x = 0; x < width; x++) {
      cb = cg = cr = ca = sum = 0;
      read = x - blurRadius;
      if (read < 0) {
        bk0 = -read;
        read = 0;
      } else {
        if (read >= width) {
          break;
        }
        bk0 = 0;
      }
      for (i = bk0; i < blurKernelSize; i++) {
        if (read >= width) {
          break;
        }
        const c = argb[read + yi];
        bm = blurMult[i];
        ca += bm[(c & -16777216) >>> 24];
        cr += bm[(c & 16711680) >> 16];
        cg += bm[(c & 65280) >> 8];
        cb += bm[c & 255];
        sum += blurKernel[i];
        read++;
      }
      ri = yi + x;
      a2[ri] = ca / sum;
      r2[ri] = cr / sum;
      g2[ri] = cg / sum;
      b2[ri] = cb / sum;
    }
    yi += width;
  }
  yi = 0;
  ym = -blurRadius;
  ymi = ym * width;
  for (y = 0; y < height; y++) {
    for (x = 0; x < width; x++) {
      cb = cg = cr = ca = sum = 0;
      if (ym < 0) {
        bk0 = ri = -ym;
        read = x;
      } else {
        if (ym >= height) {
          break;
        }
        bk0 = 0;
        ri = ym;
        read = x + ymi;
      }
      for (i = bk0; i < blurKernelSize; i++) {
        if (ri >= height) {
          break;
        }
        bm = blurMult[i];
        ca += bm[a2[read]];
        cr += bm[r2[read]];
        cg += bm[g2[read]];
        cb += bm[b2[read]];
        sum += blurKernel[i];
        ri++;
        read += width;
      }
      argb[x + yi] =
        ((ca / sum) << 24) |
        ((cr / sum) << 16) |
        ((cg / sum) << 8) |
        (cb / sum);
    }
    yi += width;
    ymi += width;
    ym++;
  }
  setPixels(pixels, argb);
}

function invertColors(pixels) {
  for (var i = 0; i < pixels.length; i+= 4) {
    pixels[i] = pixels[i] ^ 255; // Invert Red
    pixels[i+1] = pixels[i+1] ^ 255; // Invert Green
    pixels[i+2] = pixels[i+2] ^ 255; // Invert Blue
  }
}
// from https://github.com/processing/p5.js/blob/main/src/image/filters.js
function dilate(pixels, canvas) {
 let currIdx = 0;
 const maxIdx = pixels.length ? pixels.length / 4 : 0;
 const out = new Int32Array(maxIdx);
 let currRowIdx, maxRowIdx, colOrig, colOut, currLum;

 let idxRight, idxLeft, idxUp, idxDown;
 let colRight, colLeft, colUp, colDown;
 let lumRight, lumLeft, lumUp, lumDown;

 while (currIdx < maxIdx) {
   currRowIdx = currIdx;
   maxRowIdx = currIdx + canvas.width;
   while (currIdx < maxRowIdx) {
     colOrig = colOut = getARGB(pixels, currIdx);
     idxLeft = currIdx - 1;
     idxRight = currIdx + 1;
     idxUp = currIdx - canvas.width;
     idxDown = currIdx + canvas.width;

     if (idxLeft < currRowIdx) {
       idxLeft = currIdx;
     }
     if (idxRight >= maxRowIdx) {
       idxRight = currIdx;
     }
     if (idxUp < 0) {
       idxUp = 0;
     }
     if (idxDown >= maxIdx) {
       idxDown = currIdx;
     }
     colUp = getARGB(pixels, idxUp);
     colLeft = getARGB(pixels, idxLeft);
     colDown = getARGB(pixels, idxDown);
     colRight = getARGB(pixels, idxRight);

     //compute luminance
     currLum =
       77 * ((colOrig >> 16) & 0xff) +
       151 * ((colOrig >> 8) & 0xff) +
       28 * (colOrig & 0xff);
     lumLeft =
       77 * ((colLeft >> 16) & 0xff) +
       151 * ((colLeft >> 8) & 0xff) +
       28 * (colLeft & 0xff);
     lumRight =
       77 * ((colRight >> 16) & 0xff) +
       151 * ((colRight >> 8) & 0xff) +
       28 * (colRight & 0xff);
     lumUp =
       77 * ((colUp >> 16) & 0xff) +
       151 * ((colUp >> 8) & 0xff) +
       28 * (colUp & 0xff);
     lumDown =
       77 * ((colDown >> 16) & 0xff) +
       151 * ((colDown >> 8) & 0xff) +
       28 * (colDown & 0xff);

     if (lumLeft > currLum) {
       colOut = colLeft;
       currLum = lumLeft;
     }
     if (lumRight > currLum) {
       colOut = colRight;
       currLum = lumRight;
     }
     if (lumUp > currLum) {
       colOut = colUp;
       currLum = lumUp;
     }
     if (lumDown > currLum) {
       colOut = colDown;
       currLum = lumDown;
     }
     out[currIdx++] = colOut;
   }
 }
 setPixels(pixels, out);
};

function thresholdFilter(pixels, level) {
	if (level === undefined) {
      level = 0.5;
	}
	const thresh = Math.floor(level * 255);
	for (let i = 0; i < pixels.length; i += 4) {
      const r = pixels[i];
      const g = pixels[i + 1];
      const b = pixels[i + 2];
      const gray = 0.2126 * r + 0.7152 * g + 0.0722 * b;
      let val;
      if (gray >= thresh) {
        val = 255;
      } else {
        val = 0;
      }
      pixels[i] = pixels[i + 1] = pixels[i + 2] = val;
    }
  }

function ProcessToGrayImage(canvasData){

	//这个循环是取得图像的每一个点，在计算灰度后将灰度设置给原图像
        for (var x = 0; x < canvasData.width; x++) {
	    for (var y = 0; y < canvasData.height; y++) {
	        // Index of the pixel in the array
	        var idx = (x + y * canvas.width) * 4;
	        // The RGB values
	        var r = canvasData.data[idx + 0];
	        var g = canvasData.data[idx + 1];
	        var b = canvasData.data[idx + 2];
	        //更新图像数据
	        var gray = CalculateGrayValue(r , g , b);
	        canvasData.data[idx + 0] = gray;
	        canvasData.data[idx + 1] = gray;
	        canvasData.data[idx + 2] = gray;
	    }
	}
	return canvasData;
}

function CalculateGrayValue(rValue,gValue,bValue){
 	   return parseInt(rValue * 0.299 + gValue * 0.587 + bValue * 0.114);
 	}

//一维OTSU图像处理算法
function OTSUAlgorithm(canvasData){
   var m_pFstdHistogram = [];//表示灰度值的分布点概率
   var m_pFGrayAccu = [];//其中每一个值等于m_pFstdHistogram中从0到当前下标值的和
   var m_pFGrayAve = [];//其中每一值等于m_pFstdHistogram中从0到当前指定下标值*对应的下标之和
   var m_pAverage=0;//值为m_pFstdHistogram【256】中每一点的分布概率*当前下标之和
   var m_pHistogram = [];//灰度直方图
   var i,j;
   var temp=0,fMax=0;//定义一个临时变量和一个最大类间方差的值
   var nThresh = 0;//最优阀值
   //获取灰度图像的信息
   var imageInfo = GetGrayImageInfo(canvasData);
   if(imageInfo == null){
     window.alert("图像还没有转化为灰度图像！");
     return;
   }
   //初始化各项参数
   for(i=0; i<256; i++){
     m_pFstdHistogram[i] = 0;
     m_pFGrayAccu[i] = 0;
     m_pFGrayAve[i] = 0;
     m_pHistogram[i] = 0;
   }
   
   //获取图像的像素
   var pixels = canvasData.data;
   //下面统计图像的灰度分布信息
   for(i=0; i<pixels.length; i+=4){
      //获取r的像素值，因为灰度图像，r=g=b，所以取第一个即可
      var r = pixels[i];
      m_pHistogram[r]++;
   }
   //下面计算每一个灰度点在图像中出现的概率
   var size = canvasData.width * canvasData.height;
   for(i=0; i<256; i++){
      m_pFstdHistogram[i] = m_pHistogram[i] / size;
   }
   //下面开始计算m_pFGrayAccu和m_pFGrayAve和m_pAverage的值
   for(i=0; i<256; i++){
      for(j=0; j<=i; j++){
        //计算m_pFGaryAccu[256]
		m_pFGrayAccu[i] += m_pFstdHistogram[j];
		//计算m_pFGrayAve[256]
		m_pFGrayAve[i] += j * m_pFstdHistogram[j];
      }
      //计算平均值
	  m_pAverage += i * m_pFstdHistogram[i];
   }
   //下面开始就算OSTU的值，从0-255个值中分别计算ostu并寻找出最大值作为分割阀值
   for (i = 0 ; i < 256 ; i++){
		temp = (m_pAverage * m_pFGrayAccu[i] - m_pFGrayAve[i]) 
		     * (m_pAverage * m_pFGrayAccu[i] - m_pFGrayAve[i]) 
		     / (m_pFGrayAccu[i] * (1 - m_pFGrayAccu[i]));
		if (temp > fMax)
		{
			fMax = temp;
			nThresh = i;
		}
	}
   //下面执行二值化过程 
   for(i=0; i<canvasData.width; i++){
      for(j=0; j<canvasData.height; j++){
         //取得每一点的位置
         var ids = (i + j*canvasData.width)*4;
         //取得像素的R分量的值
         r = canvasData.data[ids];
         //与阀值进行比较，如果小于阀值，那么将改点置为0，否则置为255
         var gray = r>nThresh?255:0;
         canvasData.data[ids+0] = gray;
         canvasData.data[ids+1] = gray;
         canvasData.data[ids+2] = gray;
      }
   }
   return canvasData;
}	
 
 //获取图像的灰度图像的信息
function GetGrayImageInfo(canvasData){
    
	if(canvasData.data.length===0){
	  return null;
	}
	return [canvasData,context];
}
 /*下面对灰度图像进行处理，将目标信息分割出来
function DividedTarget(canvasData){
   //读取二值化图像信息
   
   
   var newVanvasData = canvasData;
   //取得图像的宽和高
   var width = canvasData.width;
   var height = canvasData.height;
   //算法开始
   var cursor = 2;
   for(var x=0; x<width; x++){
      for(var y=0; y<height; y++){
         //取得每一点的位置
         var ids = (x + y*canvasData.width)*4;
         //取得像素的R分量的值
         var r = canvasData.data[ids];
         //如果是目标点
         if(r===0){
            
         }
      }
   }
   return canvasData;
}
*/
 
function preprocessImage(processedImageData) {
  if(blurARGBVal) {blurARGB(processedImageData.data, canvas, blurARGBradius);}
  if(dilateVal ) {dilate(processedImageData.data, canvas);}
  if(invertColorsVal ) {invertColors(processedImageData.data);}
  if(thresholdFilterVal ) {thresholdFilter(processedImageData.data, thresholdLevel);}
  if(processToGrayImageVal  ) {ProcessToGrayImage(processedImageData);}
  if(oTSUAlgorithmVal ) {OTSUAlgorithm(processedImageData);}
  return processedImageData;
  }

function App(props) {

  const [ text, setText] = useState ('');
  const [ status, setStatus] = useState ('');
  const [ progress, setProgress] = useState ('');
  const [ dataUri, setDataUri] = useState ('');
  const [ filterVal, setFilterval] = useState (thresholdLevel);
  const [ radiusVal, setRadiusval] = useState (blurARGBradius);
  const handleTakePhoto = (dataUri) => {
    console.log(dataUri);
    convertURIToImageData(dataUri).then(function(imageData) {
		console.log(imageData);
		canvas.width = imageData.width;
		canvas.height = imageData.height;
		context.putImageData(preprocessImage(imageData), 0, 0); //context.putImageData(preprocessImage(imageData), 0, 0);
		dataUri = canvas.toDataURL('image/png');
		setDataUri(dataUri);
		setText("Recognizing...");
		doOCR(dataUri);

		});
   };

  const worker = createWorker({
    langPath: './',
    gzip: false,  // Indicate that the traineddata is not gzipped
    logger: m => {
      console.log(m);
      setStatus(m.status);
      setProgress(m.progress);
    },
  });

  const doOCR = async (imageData) => {
    await worker.load();
    await worker.loadLanguage('lets');
    await worker.initialize('lets');
    //await worker.setParameters({ tessedit_char_whitelist: '0123456789', });
    const { data: { text, symbols } } = await worker.recognize(imageData);
	console.log(text);
    setText(text);
	console.log(symbols);
	context.strokeStyle = "rgb(0,255,0)";
	symbols.forEach((block) => {
		context.beginPath(block.bbox.x0, block.bbox.y0);
		context.lineTo(block.bbox.x1, block.bbox.y0);
		context.lineTo(block.bbox.x1, block.bbox.y1);
		context.lineTo(block.bbox.x0, block.bbox.y1);
		context.lineTo(block.bbox.x0, block.bbox.y0);
		context.closePath();
		context.stroke();
	});
	imageData = canvas.toDataURL('image/png');
	setDataUri(imageData);
    await worker.terminate()
  };

  return (
    <div className="App">
      <h3 align="center"> 按下拍照按鈕檢測錶面數字 </h3>
	  Set Filter<p></p>
	  <input type="checkbox" id="blurARGB" name="blurARGB" onChange={(event) => {setBlurARGB(event.target.checked );}} />blurARGB 
	  <input type="checkbox" id="dilate" name="dilate" onChange={(event) => {setDilate(event.target.checked );}}  />dilate
	  <input type="checkbox" id="invertColors" name="invertColors" onChange={(event) => {setInvertColors(event.target.checked );}}  />invertColors
	  <input type="checkbox" id="thresholdFilter" name="thresholdFilter" onChange={(event) => {setThresholdFilter(event.target.checked );}}  />thresholdFilter
	  <p></p>
	  Set Filter<p></p>
	  <input type="checkbox" id="processToGrayImage" name="processToGrayImage" onChange={(event) => {setProcessToGrayImage(event.target.checked );}} />ProcessToGrayImage 
	  <input type="checkbox" id="oTSUAlgorithm" name="oTSUAlgorithm" onChange={(event) => {setOTSUAlgorithm(event.target.checked );}}  />OTSUAlgorithm
	 <p></p>
    Set Filter Level
	  <input 
		id="inFilterSlide" 
		className="custom-range"
		type="range" 
		min="0.5" max="1" 
		onChange={(event) => {setFilterval(event.target.value); setThresholdLevel(event.target.value);}}
		step="0.01"
		value = {filterVal}
	  /> {filterVal} 
	  <p></p>
	  Set Radius Level 
	  <input 
		id="inRadiusSlide" 
		className="custom-range"
		type="range" 
		min="0" max="5" 
		onChange={(event) => {setRadiusval(event.target.value); setBlurRadiusVal(event.target.value);}}
		step="0.1"
		value = {radiusVal}
	  />  {radiusVal } 
      <p></p>
        <div >
          {status || 'Waiting'} 
          <progress id="progressbar" max="1" value={progress}></progress>
        </div>
        <div style={{height:'89px', width:'352px',overflow:'hidden',border:'1px solid black'}}>
		  <img src={dataUri} alt="preview" />
        </div>
      
	  <h3 align="center"> 結果: {text || '' } </h3>
      <div style={{width: '70%', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
        <img src="./border_480640.png" style={{position: 'absolute', width: '70%', opacity: 0.7, zIndex: '1'}} alt="border" />
        <Camera style={{width:'70%', position: 'absolute', top: 0, left: 0}}
          onTakePhoto = { (dataUri) => { handleTakePhoto(dataUri);}  }
          idealFacingMode={FACING_MODES.ENVIRONMENT}
		  //idealResolution = {{width: 640, height: 480}}
          isFullscreen = {props.isFullscreen}
          isImageMirror= {props.isImageMirror}
          imageType= {props.imageType}
          onCameraStart= {props.onCameraStart}
          onCameraStop= {props.onCameraStop}
          onCameraError= {props.onCameraError}
        >
        </Camera>
      </div>
    </div>
  );
}

App.propTypes = {
  showLogsOnConsole: PropTypes.bool,
  onTextRecognize: PropTypes.func,
  isFullscreen: PropTypes.bool,
  isImageMirror: PropTypes.bool,
  imageType: PropTypes.string,
  onCameraStart: PropTypes.func,
  onCameraStop: PropTypes.func,
  onCameraError: PropTypes.func,
}

App.defaultProps = {
  showLogsOnConsole: true,
  onTextRecognize: (text) => {console.log('Text: ', text)},
  isImageMirror: false,
  isFullscreen: false,
  imageType: 'png',
  onCameraStart: () => {},
  onCameraStop: () => {},
  onCameraError: () => {},
}

export default App;
